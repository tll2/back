import requests
from aiogram import types, Bot
from fastapi import status, Request
from loguru import logger

from project.server import app
from project.settings import settings
from project.telegram import dp
from project.utils.custom_encoders import FormData


async def handle_update(update: types.Update):
    bot = Bot.get_current()
    try:
        return await dp.feed_update(bot, update)
    except BaseException as exception:
        logger.exception(exception)

# r = requests.post(f'https://api.telegram.org/bot{settings.BOT_TOKEN}/setWebhook?url=https://{settings.DOMAIN}/')
# logger.info(r.json())

# @app.post('/')
# async def webhook_handler(update: types.Update, token: str):
#     logger.info(update.message)
#     logger.info(token)
#     assert token == settings.BOT_TOKEN, (status.HTTP_418_IM_A_TEAPOT, ':(')
#     logger.info('doneeeeeeeeeeeeee')
#     return await handle_update(update)

@app.post('/')
async def webhook_handler(request: Request):
    data = await request.json()
    update = types.Update(**data)
    token = data.get("token", "")
    logger.info(f'{update.message.chat.id}, {update.message.text}')
    logger.info(token)
    # assert token == settings.BOT_TOKEN, (status.HTTP_418_IM_A_TEAPOT, ':(')
    return await handle_update(update)
# api/front/submit


@app.post("/api/front/submit")
async def handle_form_submit(data: FormData):
    # Обработка данных формы
    logger.info(data)
    # Здесь ты можешь сохранить данные в базу данных или выполнить другие действия
    return {"message": "Data received successfully", "data": data}
