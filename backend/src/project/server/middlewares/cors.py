from starlette.requests import Request
from fastapi.middleware.cors import CORSMiddleware

from project.server import app
from project.settings import settings


@app.middleware('http')
async def subdomain_cors_middleware(request: Request, call_next):
    response = await call_next(request)
    origin = request.headers.get('origin')
    if origin and origin.endswith(settings.DOMAIN):
        response.headers['Access-Control-Allow-Origin'] = origin
        response.headers['Access-Control-Allow-Methods'] = 'POST, GET, DELETE, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type, Access-Control-Allow-Headers, ' \
                                                           'Authorization, X-Requested-With'
    return response

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],  # Разрешенные источники
    allow_credentials=True,
    allow_methods=["*"],  # Разрешить все методы
    allow_headers=["*"],  # Разрешить все заголовки
)