from aiogram import types
from aiogram.filters import Command, Text
from aiogram.fsm.context import FSMContext


from project.db.models import Registry
from project.settings import settings
from project.telegram import dp
from project.telegram.utils.answer import answer
from project.telegram.utils.handler import handler


# @dp.message(Text('cancel', ignore_case=True), state="*")
# @handler
# async def cancel_handler(message: types.Message, state: FSMContext):
#     # Пользователь может отправить сообщение "cancel" в любом состоянии, чтобы отменить операцию
#     await state.clear()  # Очищаем состояние
#     await answer(chat_id=message.chat.id, text="Регистрация отменена.")


@dp.message(Command('start'))
@handler
async def start(message: types.Message, state: FSMContext):
    await answer(chat_id=message.chat.id, text='Hello, world!')


WAITING_FOR_NAME = "waiting_for_name"
WAITING_FOR_SURNAME = "waiting_for_surname"


@dp.message(Command('registration'))
@handler
async def form(message: types.Message, state: FSMContext):
    # if message.chat.id in ...:
    #     await answer(chat_id=message.chat.id, text='вы уже зарегистрированы')
    await state.set_state(WAITING_FOR_NAME)
    await answer(chat_id=message.chat.id, text="Пожалуйста, введите ваше имя:")


async def send_menu_with_webapp(user_id, total_reg, name, surname):
    # Создание кнопки для WebApp с параметрами total_reg, name, surname

    webapp_url = f'https://{settings.FRONT}/results?total_reg={total_reg}&name={name}&surname={surname}'
    web_button = types.InlineKeyboardButton(text="Open WebApp", web_app=types.WebAppInfo(url=webapp_url))

    keyboard = types.InlineKeyboardMarkup(inline_keyboard=[[web_button]])

    # Отправка сообщения с кнопкой WebApp
    await answer(chat_id=user_id, text="Регистрация завершена! Check WebApp.", reply_markup=keyboard)


@dp.message(Text(contains=""))
@handler
async def process_name(message: types.Message, state: FSMContext):
    if await state.get_state() == WAITING_FOR_NAME:
        await state.update_data(name=message.text)
        await state.set_state(WAITING_FOR_SURNAME)
        await answer(chat_id=message.chat.id, text="Теперь введите вашу фамилию:")
        return
    if await state.get_state() == WAITING_FOR_SURNAME:
        user_data = await state.get_data()
        name = user_data.get('name')
        surname = message.text
        # Сохранение в базу данных через Tortoise ORM
        user = Registry(name=name, surname=surname, chat_id=message.chat.id)
        await user.save()

        # Получение общего количества регистраций
        total_reg = await Registry.all().count()

        # Отправка запроса на фронтенд (примерный код, здесь нужно использовать HTTP-клиент)
        await send_menu_with_webapp(message.chat.id, total_reg, name, surname)

        # Завершение регистрации и отправка пользователю меню с WebApp
        await answer(chat_id=message.chat.id, text="Регистрация завершена!")

        # Выход из состояния
        await state.clear()
    else:
        await answer(chat_id=message.chat.id, text="hello!")
